package testcases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import pages.LoginPage;
import wrappers.Annotations;

public class TC_004DuplicateLeadTest extends Annotations {
	
	@BeforeTest
	
	public void setdata() {
		excelFileName="TC_004";
	}
	
	
	@Test(dataProvider="fetchData")
	public void EditLead(String Username, String Password, String EmailID, String FName) throws InterruptedException {
		
		new LoginPage()
		.enterUserName(Username)
		.enterPassword(Password)
		.clickLoginButton()
		.clickCRMLink()
		.clickLeadmenu()
		
		.ClickFindLead()
		.ClickEmailtab()
		.EnterEmailID(EmailID)
		.ClickFindLeadButton()
		.SelectFirstNameResult()
		.ClickDuplicateLead()
		.ClickCreateLead()
		.verifyFirstName(FName);
		
	}

}
