package testcases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import pages.LoginPage;
import wrappers.Annotations;

public class TC_003EditLeadTest extends Annotations {
	
	@BeforeTest
	public void setdata() {
		excelFileName="TC_003";
	}
	
	
	@Test(dataProvider="fetchData")
	public void EditLead(String Username, String Password, String Fname, String NewCompanyName) throws InterruptedException {
		
		new LoginPage()
		.enterUserName(Username)
		.enterPassword(Password)
		.clickLoginButton()
		.clickCRMLink()
		.clickLeadmenu()
		.ClickFindLead()
		.EnterFirstName(Fname)
		.ClickFindLeadButton()
		.SelectTheFirstID()
		.ClickEdit()
		.ClearCompanyName()
		.EnterNewCompanyName(NewCompanyName)
		.ClickUpdate()
		.VerifyCompanyName(NewCompanyName);
		
		
	}

}
