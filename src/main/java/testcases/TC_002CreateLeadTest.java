package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wrappers.Annotations;

public class TC_002CreateLeadTest extends Annotations{
	
	@BeforeTest
	public void setdata() {
		excelFileName="TC_002";
	}
	
	@Test(dataProvider="fetchData")
	public void CreateLead(String Username, String Password, String LoginName, String CompanyName, String FirstName, String LastName, String Email,String PhoneNumber) {
		new LoginPage()
		.enterUserName(Username)
		.enterPassword(Password)
		.clickLoginButton()
		.verifyLoginName(LoginName)
		.clickCRMLink()
		.clickLeadmenu()
		.clickCreateLead()
		.enterCompanyName(CompanyName)
		.enterFirstName(FirstName)
		.enterLastName(LastName)
		.enterEmailId(Email)
		.EnterPhoneNumber(PhoneNumber)
		.clickCreateLeadbutton()
		.verifyFirstName(FirstName);
	}

}
