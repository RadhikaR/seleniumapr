package testcases;


import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


import pages.LoginPage;
import wrappers.Annotations;

public class TC_005DeleteLeadTest extends Annotations {
	
	@BeforeTest
	public void setdata() {
		excelFileName="TC_005";
	}
	
	
	@Test(dataProvider="fetchData")
	public void EditLead(String Username, String Password, String PhoneNumber, String NewCompanyName) throws InterruptedException {
		
		new LoginPage()
		.enterUserName(Username)
		.enterPassword(Password)
		.clickLoginButton()
		.clickCRMLink()
		.clickLeadmenu()
		.ClickFindLead()
		.CLickPhonetab()
		.EnterPhoneNumber(PhoneNumber)
		.ClickFindLeadButton()
		.GetLeadId()
		.SelectTheFirstID()
		.CLickDelete()
		.ClickFindLead()
		.EnterLeadID(Lead_Id)
		.ClickFindLeadButton()
		.VerifySearchResult();
	}

}
