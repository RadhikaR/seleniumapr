package pages;

import wrappers.Annotations;

public class ViewLeadPage extends Annotations{

	public ViewLeadPage verifyFirstName(String data) {
		String FName = driver.findElementById("viewLead_firstName_sp").getText();
		
		if(FName.equals(data)) {
			System.out.println("First Name is Matching");
		}else {
			System.out.println("First Name is not matching");
		}
		
		return this;
	}
	
	public OpenTapsCRMPage ClickEdit() {
		driver.findElementByLinkText("Edit").click();
		return new OpenTapsCRMPage();
	}
	
	public ViewLeadPage VerifyCompanyName(String data) {
		String Cname = driver.findElementById("viewLead_companyName_sp").getText();
		
		if(Cname.equals(data)) {
			System.out.println("Comapny Name is Updated");
		}else {
			System.out.println("Comapany Name is not Updated");
		}
		return this;
		
			
		}
	
	public DuplicateLeadPage ClickDuplicateLead() {
		driver.findElementByLinkText("Duplicate Lead").click();
		return new DuplicateLeadPage();
	}
	
	public MyLeadsPage CLickDelete() throws InterruptedException {
		Thread.sleep(5000);
		driver.findElementByLinkText("Delete").click();
		return new MyLeadsPage();
	}
	

		
	
	}
	


