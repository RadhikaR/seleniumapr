package pages;

import wrappers.Annotations;

public class OpenTapsCRMPage extends Annotations{
	
	public OpenTapsCRMPage ClearCompanyName() {
		driver.findElementById("updateLeadForm_companyName").clear();
		return this;
	}
	
	public OpenTapsCRMPage EnterNewCompanyName(String data) {
		driver.findElementById("updateLeadForm_companyName").sendKeys(data);
		return this;
	}

	public ViewLeadPage ClickUpdate() {
		driver.findElementByXPath("//input[@value='Update']").click();
		return new ViewLeadPage();
	}
	
}
