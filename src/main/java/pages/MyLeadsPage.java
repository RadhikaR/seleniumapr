package pages;

import wrappers.Annotations;

public class MyLeadsPage extends Annotations {
	
	public CreateLeadPage clickCreateLead() {
		driver.findElementByLinkText("Create Lead").click();
		return new CreateLeadPage();
	}
	
	public FindLeadsPage ClickFindLead() {
		driver.findElementByLinkText("Find Leads").click();
		return new FindLeadsPage();
	}

}
