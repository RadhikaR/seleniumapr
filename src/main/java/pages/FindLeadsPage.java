package pages;

import wrappers.Annotations;

public class FindLeadsPage extends Annotations{
	
	
	
	public FindLeadsPage EnterFirstName(String data) {
		driver.findElementByXPath("(//input[@name='firstName'])[3]").sendKeys(data);
		return this;
	}
	public FindLeadsPage ClickFindLeadButton() throws InterruptedException {
		driver.findElementByXPath("//button[text()='Find Leads']").click();
		Thread.sleep(5000);
		return this;
	}

	public ViewLeadPage SelectTheFirstID() {
		driver.findElementByXPath("(//a[@class='linktext'])[4]").click();
		return new ViewLeadPage();
		
		}
	
	public FindLeadsPage ClickEmailtab() {
		driver.findElementByXPath("//span[text()='Email']").click();
		return this;
	}
	
	
   public FindLeadsPage EnterEmailID(String data) {
	
	driver.findElementByXPath("//input[@name='emailAddress']").sendKeys(data);
	return this;
    }
   
   public ViewLeadPage SelectFirstNameResult() {
	   driver.findElementByXPath("//div[@class='x-grid3-cell-inner x-grid3-col-firstName']/a").click();
	   return new ViewLeadPage();
   }
   
   public FindLeadsPage CLickPhonetab() {
   driver.findElementByXPath("(//span[@class='x-tab-strip-text '])[2]").click();
   return this;
   }
   
   public FindLeadsPage EnterPhoneNumber(String data) {
	   driver.findElementByXPath("//input[@name='phoneNumber']").sendKeys(data);
	   return this;
}
   
   public FindLeadsPage GetLeadId() {
	    Lead_Id = driver.findElementByXPath("(//a[@class='linktext'])[4]").getText();
	   return this;
   }
   
   public FindLeadsPage EnterLeadID(String Lead_Id) {
	   driver.findElementByXPath("//input[@name='id']").sendKeys(Lead_Id);
	   return this;
   }
   
   
   public void VerifySearchResult() {
	   String Text = driver.findElementByXPath("//div[@class='x-paging-info']").getText();
	   
	   if (Text.contains("No records")) {
		   System.out.println("No records to display");
	   }else {
		   System.out.println("Records are displaying");
	   }
   }
}
