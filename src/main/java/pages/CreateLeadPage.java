package pages;

import wrappers.Annotations;

public class CreateLeadPage extends Annotations{
	
	public CreateLeadPage enterCompanyName(String data) {
		driver.findElementById("createLeadForm_companyName").sendKeys(data);
		return this;
	}
	
	public CreateLeadPage enterFirstName(String data) {
		driver.findElementById("createLeadForm_firstName").sendKeys(data);
		return this;
	}
	
	public CreateLeadPage enterLastName(String data) {
		driver.findElementById("createLeadForm_lastName").sendKeys(data);
		return this;
	}
	
	public CreateLeadPage enterEmailId(String data) {
		driver.findElementById("createLeadForm_primaryEmail").sendKeys(data);
		return this;
	}
	
	public CreateLeadPage EnterPhoneNumber(String data) {
		driver.findElementById("createLeadForm_primaryPhoneNumber").sendKeys(data);
	    return this;
	}
	
	
	
	public ViewLeadPage clickCreateLeadbutton() {
		driver.findElementByXPath("//input[@value='Create Lead']").click();
		return new ViewLeadPage();
	}
	

}
